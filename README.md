# Overview

This gist provides a custom `flattenArray` function.
`flattenArray` function flattens an array of arbitrarily nested arrays and returns a flat array.

# Installation

I also created a [git repository](https://bitbucket.org/vpyskunov/array-flatten/) on Bitbucket where you can download all the code if you like.

1. `git clone git@bitbucket.org:vpyskunov/array-flatten.git` - Clone repository on your computer.
1. `npm install` - Install the dependencies (jest and jsdoc).
1. `npm test` - Run tests.
1. `npm run docs` - Generate documentation files.

# Documentation

Documentation is written using [JSDoc](https://jsdoc.app).
In order to generate the documentation files, run the following command in the terminal:
```
npm run docs
```
You can find the generated documentation in the `out` folder.
You can open `out/index.html` file in the browser in order to view the documentation.

# Tests

Tests are written using [Jest](https://jestjs.io).
You can find tests in `flatten-array.test.js`.
In order to run tests, you should execute the following command in the terminal:
```
npm test
```
