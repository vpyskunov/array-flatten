const { flattenArray } = require('./flatten-array.js');

test('should return empty array', () => {
    const input = [];
    const expectedOutput = [];
    expect(Array.isArray(flattenArray(input))).toEqual(true);
    expect(flattenArray(input)).toHaveLength(0);
    expect(flattenArray(input)).toEqual(expectedOutput);
});

test('should return [1, 2, 3, 4]', () => {
    const input = [[1, 2, [3]], 4];
    const expectedOutput = [1, 2, 3, 4];
    expect(Array.isArray(flattenArray(input))).toEqual(true);
    expect(flattenArray(input)).toHaveLength(4);
    expect(flattenArray(input)).toEqual(expectedOutput);
});

test('should return [0]', () => {
    const input = [0];
    const expectedOutput = [0];
    expect(Array.isArray(flattenArray(input))).toEqual(true);
    expect(flattenArray(input)).toHaveLength(1);
    expect(flattenArray(input)).toEqual(expectedOutput);
});

test('should return [1]', () => {
    const input = [[[[[1]]]]];
    const expectedOutput = [1];
    expect(Array.isArray(flattenArray(input))).toEqual(true);
    expect(flattenArray(input)).toHaveLength(1);
    expect(flattenArray(input)).toEqual(expectedOutput);
});

test('should return [1, 2, 3, 4]', () => {
    const input = [[[1, 2], [3]], [[4]]];
    const expectedOutput = [1, 2, 3, 4];
    expect(Array.isArray(flattenArray(input))).toEqual(true);
    expect(flattenArray(input)).toHaveLength(4);
    expect(flattenArray(input)).toEqual(expectedOutput);
});

test('should throw an error if invoked without arguments', () => {
    expect(() => flattenArray()).toThrow(
        new Error('You should provide at least 1 argument')
    );
});

test('should throw an error if an argument is not an array', () => {
    const input = 'string';
    expect(() => flattenArray(input)).toThrow(
        new TypeError('inputArray argument should be an array')
    );
});