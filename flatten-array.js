/**
 * Flattens an array of arbitrarily nested arrays into a flat array
 * @param {Array} inputArray - Array of arbitrarily nested arrays
 * @returns {Array} Flat array
 * @throws {TypeError} inputArray argument should be an array
 * @throws {Error} You should provide at least 1 argument
 */
const flattenArray = (inputArray) => {
    if (inputArray === undefined) {
        throw new Error('You should provide at least 1 argument');
    }
    
    if (!Array.isArray(inputArray)) {
        throw new TypeError('inputArray argument should be an array');
    }
    
    let resultingArray = [];
    inputArray.forEach(item => {
        if (Array.isArray(item)) {
            resultingArray = resultingArray.concat(flattenArray(item));
        } else {
            resultingArray.push(item);
        }
    });
    
    return resultingArray;
}

module.exports = {
    flattenArray 
};
